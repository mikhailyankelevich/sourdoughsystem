import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Database {
	private String connectionString = "jdbc:mysql://localhost:3306/lab2";
	private String login = "root";
	private String password = "Vepop05121998";
	private String dbName = "test";
	private String table1 = "testmemory";
	private Connection c;

	/*
	 * these methods are just regular get functions of the values above
	 */
	public Connection getConnection() {
		return c;
	}

	public String getNameDb() {
		return dbName;
	}

	public String getTableName() {
		return table1;
	}

	/*
	 * this is a constructor, which is connecting to the database
	 */
	public Database() throws SQLException {
		c = DriverManager.getConnection(this.connectionString, login, password); // open the database
	}

	/*
	 * this method shuts down the server
	 */
	public void shutDown() throws SQLException {
		c = DriverManager.getConnection(connectionString + ";shutdown=true", login, password);// shutting down database(
																								// may be useful for not
																								// MYSQL
	}

	/*
	 * this Method inserts data with just a date
	 */
	public void insertData(long DATEPUT) throws SQLException {

		String sql = "INSERT INTO `" + dbName + "`.`" + table1 + "` ( `date`) " + "VALUES (" + DATEPUT + ");";
		c.createStatement().executeUpdate(sql);
//		 c.commit(); //no need as autocommit is on 

	}

	/*
	 * this Method inserts data with both id and a date
	 */
	public void insertData(int ID, long DATEPUT) throws SQLException {

		String sql = "INSERT INTO `" + dbName + "`.`" + table1 + "` ( `IdBox`,`date`) " + "VALUES (" + ID + ","
				+ DATEPUT + ");";
		c.createStatement().executeUpdate(sql);
//		 c.commit(); //no need as autocommit is on 

	}

	/*
	 * this method closes the database( especially the local ones or it will not
	 * save)
	 */
	public void close() throws SQLException { // don't forget to close the database, especially local ones
		c.close();
	}

	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {

		String toReturn = "";

		PreparedStatement pst;
		try {
			pst = c.prepareStatement("select * from `" + dbName + "`.`" + table1 + "`");
			pst.clearParameters();
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				toReturn += rs.getString(1) + " " + rs.getString(2) + " " + "\n";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("nothing saved:(");
			return null;
		}

		return toReturn;
	}

	
	/*
	 * this method removes tuple
	 */
	public void remove(int id) throws SQLException {

		String sql = "DELETE FROM `"+ dbName+"`.`"+table1+"` WHERE (`IdBox` = '"+id+"');";
		c.createStatement().executeUpdate(sql);
	}
	
	
	

	public static void main(String[] src) {
		try {
			Database dat = new Database();
//			Random rand=new Random();
			dat.insertData(new Date().getTime()  );
//			dat.remove(22);

			System.out.println(dat);
			dat.close();
			System.out.println("finish");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("shit");
			e.printStackTrace();
		}

	}

}