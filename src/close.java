import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class close  {

 /*
  * this method shows alert which will ask if program should close
  */
   public boolean showConfirmation() {
 
      Alert alert = new Alert(AlertType.CONFIRMATION);
      alert.setTitle("Close Window");
      alert.setHeaderText("Вы уверены, что хотите  закрыть программу?");
      Optional<ButtonType> option = alert.showAndWait();
      if (option.get() == ButtonType.OK) 
    	  return true;
      return false;
   }

 
}