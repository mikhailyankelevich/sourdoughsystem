public class millSecConverter{
	private Long milSec;
	private static final int SECOND = 1000;
	private static final int MINUTE = 60 * SECOND;
	private static final int HOUR = 60 * MINUTE;
	private static final int DAY = 24 * HOUR;
	
	 /*
	  * this is a constructor getting input 
	  */
	public millSecConverter(Long input) {
		milSec=input;
	}
	
	/*
	 * this method returns time in d/g/min
	 */
	public String toDays() {

		long milSec = 10304004543l;
		StringBuffer text = new StringBuffer("");
		if (milSec > DAY) {
		  text.append(milSec / DAY).append(" days ");
		  milSec %= DAY;
		}
		if (milSec > HOUR) {
		  text.append(milSec / HOUR).append(" hours ");
		  milSec %= HOUR;
		}
		if (milSec > MINUTE) {
		  text.append(milSec / MINUTE).append(" minutes ");
		  milSec %= MINUTE;
		}
		return text.toString();
	}
	
	
	/*
	 * this method returns time in g/min
	 */
	public String toHours() {
		StringBuffer text = new StringBuffer("");
		if (milSec > HOUR) {
		  text.append(milSec / HOUR).append(":");
		  milSec %= HOUR;
		}
		if (milSec > MINUTE) {
		  text.append(milSec / MINUTE).append("");
		  milSec %= MINUTE;
		}
		return text.toString();
		
	}
	
	public static void main (String[] crs) {
		new millSecConverter(10304004543l).toHours();
	}
}