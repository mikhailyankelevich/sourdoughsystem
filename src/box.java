
import java.text.SimpleDateFormat;
import java.util.Date;

public class box implements Comparable<box> {
	private int idBox;
	private Long date;
	private String dateLength;
	private String dateS;
	@SuppressWarnings("unused")
	private Long sorageTimeLength = new Long(3600000 * (new visual().numberOfHoursToHold - 1));

	public box(int idBox) {
		this.idBox = idBox;
		this.date = new Date().getTime();
		SimpleDateFormat df2 = new SimpleDateFormat("yy/MM/dd HH:mm");
		dateLength = new millSecConverter(new Date().getTime() - this.date).toHours();
		dateS = df2.format(new Date(this.date));
	}

	public box(int idBox, Long date) {
		this.idBox = idBox;
		this.date = date;
		SimpleDateFormat df2 = new SimpleDateFormat("yy/MM/dd HH:mm");
		dateLength = new millSecConverter(new Date().getTime() - this.date).toHours();
		dateS = df2.format(new Date(this.date));
	}

	public void setIdBox(int idBox) {
		this.idBox = idBox;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public int getIdBox() {
		return idBox;
	}

	public String getDateS() {
		return this.dateS;
	}

	public String getDateLength() {
		return this.dateLength;
	}

	public Long getDate() {
		return date;
	}

	public Long timeLeft() {
		return new Date().getTime() - this.date;
	}

	@Override
	public int compareTo(box o) {
		// TODO Auto-generated method stub
		return Long.compare(this.date, o.date);
	}

	public String toString() {
		return this.idBox + "\t" + this.date + "\t";

	}

}