import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class visual extends Application implements Initializable, EventHandler<KeyEvent> {

	public static void main(String[] src) {
		launch(src);

	}

	@FXML
	TableView<box> table1;
	@FXML
	TableColumn<box, String> boxid;
	@FXML
	TableColumn<box, String> time;
	@FXML
	TableView<box> table11;
	@FXML
	TableColumn<box, String> boxid1;
	@FXML
	TableColumn<box, String> time1;
	@FXML
	TableView<box> table12;
	@FXML
	TableColumn<box, String> boxid2;
	@FXML
	TableColumn<box, String> time2;
	@FXML
	TextArea clock;
	boolean running;

	Stage window;

	public void start(Stage primaryStage) throws Exception {
		window = primaryStage;
		debug now = new debug();
		now.print("parent init\n");

		Parent myPane = FXMLLoader.load(getClass().getResource("visual.fxml"));
		now.print("title\n");

		window.setTitle("SourDough System");

		Scene myScene = new Scene(myPane);
		now.print("scene\n");

		myScene.setOnKeyPressed(this);
		myScene.setOnMouseClicked(null);
//		myScene.set

		window.setScene(myScene);
		now.print("show\n");
		window.show();

		window.setOnCloseRequest(e -> {
			e.consume();
			closeProgram();
		});
	}

	StringBuffer barcodeString = new StringBuffer("");

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.event.EventHandler#handle(javafx.event.Event) this method handles
	 * keys pressed while window is open just an experiment
	 */
	public void handle(KeyEvent event) {
		// TODO Auto-generated method stub
		
		if (event.getCode().equals(KeyCode.ENTER)) {
			System.out.println(barcodeString);
//			try {
//				int value= Integer.valueOf(barcodeString.toString()).intValue();
//				boxController.exportAndAdd(value, list);
//				barcodeString = new StringBuffer("");
//			} catch (NumberFormatException | SQLException e) {
//				// TODO Auto-generated catch block
////				e.printStackTrace();
//				barcodeString = new StringBuffer("");
//			}
			
		} else {
			KeyCode kc = event.getCode();
			barcodeString.append(kc.getName());
		}

	}

	/*
	 * This method closes application. It asks for approval and closes app ONLY when
	 * action is approved.
	 */
	public void closeProgram() {
		if (new close().showConfirmation()) {
			this.running = false;
			window.close();
			System.exit(0);
		}

	}

	/*
	 * This is initialize method. goes after loading fxml file for the main scene
	 * (non-Javadoc)
	 * 
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */

	BoxDataController boxController = new BoxDataController();
	List<box> list;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		running = true;
		new debug().print("initialize\n");

		list = boxController.scanDatabase();
		setMainTablesUp(list);
		runTime();
		scanBarcodeThread();

	}
	/*
	 * this method prints table with the date being the date of the box planting
	 */

	@SuppressWarnings("unchecked")
	public void printTable(List<box> list, TableView<box> table, TableColumn<box, String> boxid,
			TableColumn<box, String> time) {
		ObservableList<box> listOutput = FXCollections.observableList(list);

		boxid.setCellValueFactory(new PropertyValueFactory<box, String>("idBox"));
		time.setCellValueFactory(new PropertyValueFactory<box, String>("dateS"));

		table.getItems().setAll(listOutput);
	}

	/*
	 * this is override method of the above one, prints how much time left since
	 * planting
	 */
	public void printTable(List<box> list, TableView<box> table, String dateType, TableColumn<box, String> boxid,
			TableColumn<box, String> time) {
		ObservableList<box> listOutput = FXCollections.observableList(list);

		boxid.setCellValueFactory(new PropertyValueFactory<box, String>("idBox"));
		time.setCellValueFactory(new PropertyValueFactory<box, String>(dateType));// the string with the value name
																					// should be inserted in the
																					// function call

		table.getItems().setAll(listOutput);
	}

	/*
	 * This method sets 2 left tables up with the stock available to use therefore,
	 * only fresh
	 */
	public void setMainTablesUp(List<box> list) {
		List<box> list1 = new LinkedList<box>();
		List<box> list0 = new LinkedList<box>();
		int i = 0;
		int taleSize = 40;
		if (list.size() <= taleSize) {
			printTable(list, table1, this.boxid, this.time);
		} else {
			for (i = 0; i < taleSize; i++)
				list0.add(list.get(i));
			printTable(list0, table1, this.boxid, this.time);// 30 rows on 1 page
			for (i = i; i < list.size(); i++)
				list1.add(list.get(i));
			printTable(list1, table11, this.boxid1, this.time1);
		}
	}

	/*
	 * this is a method which starts a thread which counts time and all updates the
	 * delay between loops is 0.1 sec and every 10 sec database gets rescanned this
	 * method updates time, main tables, tables with the latest output and overdue
	 * table
	 */
	public void runTime() {

		new Thread() {
			public void run() {
				int count = 0;
				setMainTablesUp(boxController.scanDatabase());// setting up the main tables scanning the database
																// beforehand
				while (running) {
					setTime();// setting up the time
					setIncertionTime();
					count++;
					count = count % 100;
					try {
						this.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block

					}
					setMainTablesUp(boxController.getList());
					// every 100 iteration update the table and set next to extract
					if (count == 99) {
						nextToExtract(boxController.scanDatabase());

					}
				}
			}
		}.start();

	}

	/*
	 * this function gets and sets current time on the application clock
	 */
	public void setTime() {

		new debug().print(new Date().toString());
		SimpleDateFormat df2 = new SimpleDateFormat("Время: HH:mm:ss Дата: MM/dd");

		clock.setText(df2.format(new Date()));

	}

	@FXML
	TextArea FirstToTakeOut = new TextArea();
	@FXML
	TextArea TimeRemaining = new TextArea();
	@FXML
	TextArea FirstToTakeOut1 = new TextArea();
	@FXML
	TextArea TimeRemaining1 = new TextArea();
	@FXML
	TextArea FirstToTakeOut2 = new TextArea();
	@FXML
	TextArea TimeRemaining2 = new TextArea();
	@FXML
	TextArea FirstToTakeOut3 = new TextArea();
	@FXML
	TextArea TimeRemaining3 = new TextArea();
	boolean change = true;
	box prev;
	int numberOfHoursToHold = 25;

	/*
	 * This method sets up next to extract table In addition removes all overdue
	 * boxes, which it shows in the table on the bottom
	 */
	public void nextToExtract(LinkedList<box> list) {
		Long sorageTimeLength = new Long(3600000 * (numberOfHoursToHold - 1));
		Date now = new Date();
		List<box> listOld = new LinkedList<box>();
		SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");

		int size = list.size();

		while (true && list.size() > 0) {
			if ((sorageTimeLength > (now.getTime() - list.get(0).getDate()))) {
				break;
			} else {
				listOld.add(list.removeFirst());
			}
		}

		if (size != 0)
			setTableGone(listOld);

		if (list.size() > 0) {
			box b = list.get(0);
			Integer tmpID = b.getIdBox();
			this.FirstToTakeOut.setText(tmpID.toString());
			this.TimeRemaining.setText(df2.format(new Date(sorageTimeLength - now.getTime() + b.getDate())));

			if (sorageTimeLength - now.getTime() + b.getDate() < 3600000) {// starts the alarm 2 hours before the
																			// extraction deadline
				// Painting the row in red
				this.FirstToTakeOut.setStyle("-fx-control-inner-background:#800000");
				this.TimeRemaining.setStyle("-fx-control-inner-background:#800000");

				if (change == true && b != prev)// checking if sound should be played
//					new soundPlayer().play("police.wav", 5 * 10);

				change = false;// setting the sound playing to false, so it won't start every 10 sec
				prev = b;
			} else {
				if (sorageTimeLength - now.getTime() + b.getDate() > 3600000)
					change = true;
				// Painting the row in grey
				this.FirstToTakeOut.setStyle("-fx-control-inner-background:#F0EFEF");
				this.TimeRemaining.setStyle("-fx-control-inner-background:#F0EFEF");

			}
		}

		// setting up row 2 and so on as above
		// but with no css
		if (list.size() > 1) {
			box b1 = list.get(1);
			Integer tmpID1 = b1.getIdBox();
			this.FirstToTakeOut1.setText(tmpID1.toString());
			this.TimeRemaining1.setText(df2.format(new Date(sorageTimeLength - (now.getTime() - b1.getDate()))));
		}
		if (list.size() > 2) {
			box b2 = list.get(2);
			Integer tmpID2 = b2.getIdBox();
			this.FirstToTakeOut2.setText(tmpID2.toString());
			this.TimeRemaining2.setText(df2.format(new Date(sorageTimeLength - (now.getTime() - b2.getDate()))));
		}
		if (list.size() > 3) {
			box b3 = list.get(3);
			Integer tmpID3 = b3.getIdBox();
			this.FirstToTakeOut3.setText(tmpID3.toString());
			this.TimeRemaining3.setText(df2.format(new Date(sorageTimeLength - (now.getTime() - b3.getDate()))));
		}
	}

	@SuppressWarnings("rawtypes")
	@FXML
	TableView OverdueBox;
	@FXML
	TableColumn<box, String> OverdueBoxID;
	@FXML
	TableColumn<box, String> OverdueBoxTime;

	/*
	 * this method sets up the table with old boxes
	 */
	public void setTableGone(List<box> listOld) {
		printTable(listOld, OverdueBox, "dateLength", this.OverdueBoxID, this.OverdueBoxTime);
	}

	@FXML
	TextArea incertionTime;
	int[] extractionTimeMinutes = { 0, 20, 40 };
//	boolean changeToAnotherStyle = false;
	long howLongToAlarmIncertionMin = 2;

	/*
	 * this method sets up the insertion time based on the user setup
	 */
	public void setIncertionTime() {

		SimpleDateFormat df2 = new SimpleDateFormat("След Загрузка:\nHH:mm");
		Calendar cal = new GregorianCalendar(); // Current date and time
		Calendar now = new GregorianCalendar();
		long howLongToAlarmIncertion = howLongToAlarmIncertionMin * 60000;
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		int minutes = cal.get(Calendar.MINUTE);
		for (int i = 0; i < extractionTimeMinutes.length; i++) {
			if (minutes <= extractionTimeMinutes[i] - 2) {
				cal.set(Calendar.MINUTE, extractionTimeMinutes[i]);
				break;
			} else if (i == extractionTimeMinutes.length - 1) {
				cal.set(Calendar.MINUTE, extractionTimeMinutes[0]);
				cal.add(Calendar.HOUR, 1);
				break;
			}

		}

//		System.out.println(df2.format(now.getTime())+" "+df2.format(cal.getTime()));
		if (now.getTimeInMillis() < (cal.getTimeInMillis() + howLongToAlarmIncertion)
				&& now.getTimeInMillis() > (cal.getTimeInMillis() - howLongToAlarmIncertion)) {
			this.incertionTime.setStyle("-fx-control-inner-background:#800000");
		} else
			this.incertionTime.setStyle("-fx-control-inner-background:#F0EFEF");

		incertionTime.setText(df2.format(cal.getTime()));
	}

	/*
	 * this method starts a thread which will scan for input from the brocade
	 * scanner !!!!!!!!!!!TO BE ADDED!!!!!!!!!!!!!!
	 */
	@SuppressWarnings("resource")
	public void scanBarcodeThread() {

//		new Thread() {
//			public void run() {
//				Scanner scan = new Scanner(System.in);
//				while (running) {
//					new debug().print("scanning value");
//					System.out.println("scanning");
//					
////					scan.next();
//					int value= Integer.valueOf(scan.next()).intValue();
//					try {
//						boxController.exportAndAdd(value);
//					} catch (SQLException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
////					new close().showConfirmation();
//					System.out.println("scanning");
//					try {
//						this.sleep(100);
//					} catch (InterruptedException e) {
//					}
//				}
//			}
//		}.start();

	}

}
