import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class BoxDataController{
	public static void main( String[] crs) {
		visual.main(crs);
	}
	
	
	
	/*
	 * this method gets state of the database in case of the debug being on
	 */
	public void printState() {
		LinkedList<box> list=scanDatabase();
		
		for (int i=0; i<list.size();i++) {
			new debug().print(list.get(i).getIdBox()+"\t"+list.get(i).getDate()+"\t");
		}
	}
	
	/*
	 * this method scans the database and saves the data in the priority queue
	 * later converts priority queue to the Linked list,which is used later in the program
	 */
	Database dat;
	private LinkedList<box> list;
	private LinkedList<box> listFull;
	public LinkedList<box> scanDatabase() {
		try {
			dat=new Database();
				
				PriorityQueue<box> p = new PriorityQueue<box>();
				PreparedStatement pst;
				pst = dat.getConnection().prepareStatement("select * from `"+dat.getNameDb()+"`.`"+dat.getTableName()
						+"`" );
				pst.clearParameters();
				ResultSet rs = pst.executeQuery();
				while(rs.next()){
					p.add(new box(rs.getInt(1),rs.getLong(2)));
				}
				
				
				
				this.list=new LinkedList<box>();
				this.listFull=new LinkedList<box>();
				while (!p.isEmpty()) {
					listFull.add(p.peek());
					list.add(p.remove());
					new debug().print(list.getLast().getIdBox()+"\t"+list.getLast().getDate()+"\t");
				}
				return list;
			} catch (SQLException e) {
					// TODO Auto-generated catch block
					System.out.println("nothing saved:(");
				}
		return null;
	}

	
	
	/*
	 * this method returns the list without scanning
	 */
	public LinkedList<box> getList(){
	return list;
	}
	
	/*
	 * this method deletes a tuple if given id
	 */
	public void deleteTuple(int boxID) throws SQLException {
		dat.remove(boxID);
	}
	
	
	/*
	 * 
	 */
	private int deletionCount=0;
	int oldBoxID;
	int numberOfHoursToHold=24;
	public boolean exportAndAdd(int boxID) throws SQLException {
		box b=listContainsID(boxID);
		Date now=new Date();
		Long sorageTimeLength = new Long(3600000 * (numberOfHoursToHold - 1));
		if (b!=null) {
			if(sorageTimeLength - now.getTime() + b.getDate() <= 3600000*(2-1)) {
				deleteTuple(boxID);
				this.deletionCount=0;
			}
			else 
			{
				if (oldBoxID!=boxID) {
					this.deletionCount=0;
					}
//				this.deletionCount++;
				if (this.deletionCount==0) {
//					playSound
					System.out.println("deleting - "+deletionCount);
					oldBoxID=boxID;
					this.deletionCount+=1;
					return false;
				}else if (this.deletionCount==1) {
//					playSound
					System.out.println("deleting - "+deletionCount);
					this.deletionCount+=1;
					oldBoxID=boxID;
					return false;
				}else if (this.deletionCount==2) {
//					playSound
					System.out.println("deleting - "+deletionCount);
					this.deletionCount+=1;
					oldBoxID=boxID;
					return false;
				}else if (this.deletionCount==3){
					deleteTuple(boxID);
					System.out.println("deleting - "+deletionCount);
					oldBoxID=boxID;
					this.deletionCount=0;
				}
				
			}
		}else if(b==null) {
			dat.insertData(boxID,now.getTime());
			this.deletionCount=0;
			System.out.println("adding");
			this.deletionCount=0;
		}
		return true;
	}
	
	/*
	 * this method is private  ad used in only 1 function
	 * It returns a box with the given id 
	 * or if there is no such box returns null
	 */
	private box listContainsID(int boxID) {
		for (int i=0;i<listFull.size();i++) {
			System.out.println(i);
			if(listFull.get(i).getIdBox()==boxID)
				return listFull.get(i);
		}
		return null;
	}

	
	
	
	
	
}